An automated Arch Linux Assisted Installer written in Bash (Shell Script).


## Prepairing environment (Initial Setup)


### Loading your Keymap
1. The default console keymap is US. Available layouts can be easily searched with:


		localectl list-keymaps | grep -i your_contury_abbreviation_sign
		ex: localectl list-keymaps | grep -i pt


2. Then you are going to get a lot of keyboard layouts based on your country code. load your desired keyboard from the list obtained from the command above with:


		loadkeys "chosen keymap"
		ex: loadkeys pt-latin9


### Connecting to the Internet
This depends on the setup you are using, I recommend you install Arch Linux with a wired connection.


* If you are going to use a WiFi connection then type `wifi-menu` and then a bunch of wireless networks will pop-up and you choose your desired one and then you insert it's password and confirm. After this if connection is sucessfull type `dhcpcd`.


* If you are using a wired connection just type dhcpcd and you will automatically have network connection.


### Prepairing the disks for the installation
Now, we are going to the part of the setup where we actually prepare the disks for a internal installation (All the produced steps were all efectued as live USB/CD).


1. Type `fdisk /dev/sda`


* Type `o`. This will clear out any partitions on the drive.
* Now we are going to create a SWAP partition by typing `n`, then `p` for primary partition on the drive, type `1` for first partition and then press ENTER to accept the default first sector, then type `+6G` [You can type whatever the size of the SWAP partition you want by replacing the 6 with the number of the size of your desired SWAP partition in GB]
* Type `t`, then type `82` for SWAP partition type
* Type `n` then `p` for primary, 2 for the second partition, and then press ENTER twice to accept the default first and last sector.
* Now write your changes to the disk by typing `w`


2. Formating the disks and mounting them.

To format and mount your disks type these commands and accept any messages:


* `mkswap /dev/sda1`
* `mkfs.ext4 /dev/sda2`
* `swapon /dev/sda1`
* `mount /dev/sda2 /mnt`


### Installing the system


We are now going to select the main mirrors to make a fast download and then install all the systemfiles to have a minimal installation of arch to run AAI.sh


1. Downloading packages


* Type `pacstrap -i /mnt base base-devel git`.
Now your system is going to start to download, wait until it is finished.


2. Generating Fstab file


* Type `genfstab -U /mnt >> /mnt/etc/fstab`


3. Chrooting and running AAI.sh


* Type `arch-chroot /mnt`
You are now inside of your arch linux installation and inside your disk.
* Type `cd tmp && git clone https://gitlab.com/rafa_99/ai`
* Type `bash /tmp/ai/Install`
Now you are going to run the `Install` script to automatically detect your distro and install all the minimal requirements, you will have multiple customizations in terms of software availability to choose installing or not. After you get a message saying "DONE", you may proceed.
* Type `exit`
Your are now going to quit of the chrooting process
* Type `reboot` to boot into your fresh new Arch Installation ;)

## Summarized Install
### Portuguese Example with Wired Connection

1. `loadkeys pt-latin9`
2. `dhcpcd`
3. `fdisk /dev/sda`
4. `o, n, p, 1, ENTER, +6G, t, 82, n, p, 2, ENTER, ENTER, w`
5. `mkswap /dev/sda1` 
6. `mkfs.ext4 /dev/sda2`
7. `swapon /dev/sda1`
8. `mount /dev/sda2`
9. `pacstrap -i /mnt base base-devel git`
10. `genfstab -U /mnt >> /mnt/etc/fstab`
11. `arch-chroot /mnt`
12. `cd tmp && git clone https://gitlab.com/rafa_99/ai`
13. `bash /tmp/ai/Install`
14. `exit`
15. `reboot`
